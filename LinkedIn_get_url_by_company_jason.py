# def install(package):
#     subprocess.check_call([sys.executable, "-m", "pip", "install", package])
# install('pymysql')
import importlib
import os
import re
from time import sleep

import numpy
from selenium import webdriver
from bs4 import BeautifulSoup
from webdriver_manager.chrome import ChromeDriverManager
import pandas as pd
import glob
import linkedin_tools
importlib.reload(linkedin_tools)

sleep_time = 1

user_name = 'jasonandriessen@outlook.com'
password = 'Jesus123'
invite_message = ' \n\nJason Andriessen from CoreData here - I publish research insights as frequently as I can about practice management, consumer behaviour and the future of financial planning - be great to connect with you. \n\nWith kind regards \nJason '
invite_search_key_words = 'Financial%20Adviser'
connection_list_csv='connections_to_add_FPA.csv'
path='connections_added_jason'

user_name = 'brucefeiwang@gmail.com'
password = 'asdF8489721'


driver = linkedin_tools.login_to_linkedin(user_name,password)

FPA = pd.read_excel (r'FPA - RI Follow up list 22nd Sep 2020.xlsx')
FPA["Company Name"]



url_finished = os.listdir('data/LinkedIn url by company')
url_finished = [w.replace('.csv', '') for w in url_finished]

FPA = FPA[~ FPA["Company Name"].isin(url_finished)]
FPA = FPA.reset_index()

for c in range(99, 200):
    try:
        sleep(sleep_time)

        company_name_raw = FPA["Company Name"][c]
        company_name_raw = re.sub('[^A-Za-z0-9]+', ' ', company_name_raw)

        company_name = company_name_raw.replace(' Pty Ltd','').replace(' ','%20')

        driver.get('https://www.linkedin.com/search/results/companies/?keywords='+company_name+'&origin=GLOBAL_SEARCH_HEADER')

        soup = BeautifulSoup(driver.page_source, 'html.parser')
        sleep(sleep_time)

        company_url = soup.find_all('a', attrs={'data-entity-action-source': 'actor'})[0].attrs['href']

        driver.get(company_url+'people/')
        sleep(sleep_time)
        soup = BeautifulSoup(driver.page_source, 'html.parser')

        num_employees = soup.find_all('div', attrs={'class':'display-flex full-width justify-space-between align-items-center pt5 ph5'})[0].text.replace('employees','').strip()


        company_title_linkedin = soup.find_all('h1', attrs={'class':'org-top-card-summary__title t-24 t-black truncate'})[0].text.strip()

        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        sleep(sleep_time)
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        sleep(sleep_time)

        soup = BeautifulSoup(driver.page_source, 'html.parser')
        sleep(sleep_time)

        url = soup.find_all('a', attrs={'data-control-name':'people_profile_card_name_link'})
        title = soup.find_all('div', attrs={'class':'lt-line-clamp lt-line-clamp--multi-line ember-view'})
        df = pd.DataFrame()

        for i in range(1, len(url)):
            df = df.append(pd.DataFrame([[url[i].text.strip(), title[i].text.strip(),company_name_raw,company_title_linkedin, url[i].attrs['href']]], columns=['full_name', 'job_title', 'company', 'company_linkedin', 'linkedin_url']))
        if len(df) >0 :
            df.to_csv('data/LinkedIn url by company/' + company_name_raw + '.csv')
    except:
        print('error')





# merge all csvs ----
path = r'data/LinkedIn url by company' # use your path
all_files = glob.glob(path + "/*.csv")

df_merge = pd.DataFrame()

for filename in all_files:
    df = pd.read_csv(filename, index_col=None, header=0)
    df_merge = df_merge.append(df)

df_merge.to_csv('data/LinkedIn url by company.csv')


