# def install(package):
#     subprocess.check_call([sys.executable, "-m", "pip", "install", package])
# install('pymysql')
import importlib
import os

from time import sleep
from selenium import webdriver
from bs4 import BeautifulSoup
from webdriver_manager.chrome import ChromeDriverManager
import pandas as pd

import linkedin_tools
importlib.reload(linkedin_tools)

user_name = 'andrew@coredata.com.au'
password = 'Kismet5898'
invite_message = ' \n\nAndrew Inwood from CoreData here - I publish as frequently as I can about financial planning, economics and consumer behaviour on linked in - be great to connect with you. \n\nWith kind regards \nAndrew '
invite_search_key_words = 'Financial%20Adviser'
connection_list_csv = 'connections_to_add.csv'
path = 'connections_added_andrew'

driver = linkedin_tools.login_to_linkedin(user_name, password)

connections_to_add = pd.read_csv("data/Anubis_urls_to_add/" + connection_list_csv, encoding='latin1')
url_finished = os.listdir('data/Anubis_urls_to_add/' + path + '/')
url_finished = [w.replace('.csv', '') for w in url_finished]

connections_to_add = connections_to_add[~ connections_to_add['linkedin_url'].isin(url_finished)]
connections_to_add = connections_to_add.reset_index()

sleep_time = 1
number_of_invite_sent = 0

for p in range(0, 50):
    print('loop no:' + str(p))
    sleep(sleep_time)
    url_individual = connections_to_add.linkedin_url[p]
    linkedin_tools.add_connection_by_url(driver, url_individual, path, invite_message, sleep_time)
