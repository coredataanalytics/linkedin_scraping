library(dplyr)
library(data.table)
library(rvest)
library(urltools)
library(tm)
library(parallel)
library(RMySQL)


Connections_scraped_merged <- read.csv('data/andrew@coredata.com.au/Connections_scraped_merged.csv',stringsAsFactors = F)
unique_name <- names(table(Connections_scraped_merged$name))[table(Connections_scraped_merged$name)==1]
Connections_scraped_merged <- Connections_scraped_merged[Connections_scraped_merged$name %in% unique_name,]

Connections <- read.csv('data/andrew@coredata.com.au/Connections.csv',stringsAsFactors = F)
Connections <- Connections[Connections$First.Name!='',]
names <- colnames(Connections)
Connections$name <- paste0(Connections$First.Name, ' ', Connections$Last.Name)


Connections <- merge(Connections,select(Connections_scraped_merged, name, linkedin_url) , by='name',all.x=TRUE)
Connections$linkedin_url <- paste0('/in/',Connections$linkedin_url,'/')
Connections$linkedin_url[Connections$linkedin_url=='/in/NA/'] <- ''
Connections <- Connections[,c(names,'linkedin_url')]

write.csv(Connections, 'data/andrew@coredata.com.au/Connections2.csv',row.names = F)




