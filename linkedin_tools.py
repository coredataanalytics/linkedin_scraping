
from selenium import webdriver
from time import sleep
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup

def login_to_linkedin(user_name,password):
    chrome_options = webdriver.ChromeOptions();

    chrome_options.add_argument("--start-maximized")

    chrome_options.add_experimental_option("excludeSwitches", ['enable-automation']);
    # driver = webdriver.Chrome(ChromeDriverManager().install(), options=chrome_options)
    driver = webdriver.Chrome('chromedriver', options=chrome_options)

    # Login
    sleep(0.5)

    driver.get('https://www.linkedin.com/')
    sleep(0.5)
    #
    session_key = driver.find_element_by_name("session_key")
    session_key.send_keys(user_name)
    sleep(1)

    session_password = driver.find_element_by_name('session_password')
    session_password.send_keys(password)
    sleep(1)

    sign_in_button = driver.find_elements_by_css_selector("[class='sign-in-form__submit-button']")
    sign_in_button[0].click()
    sleep(2)
    return driver

def add_connection_by_url(driver,url_individual,path,invite_message,sleep_time):
    sleep(sleep_time)
    # url_individual = 'https://www.linkedin.com/in/adrian-maher-443b0059'

    try:
        driver.get('https://www.linkedin.com/in/' + url_individual)

        sleep(sleep_time)

        #  Add people with custom text ----
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        first_name = soup.find_all('li', attrs={'class': 'inline t-24 t-black t-normal break-words'})[0].text.strip().split(' ')[0]
        distance =''
        try:
            distance = soup.find_all('span', attrs={'class': 'distance-badge separator'})[0].text.strip().split(' ')[0]
        except:
            print('no distance badge')
        if distance != '1st':
            button_connect = ''
            button_connect = driver.find_elements_by_css_selector(
                "[class='pv-s-profile-actions pv-s-profile-actions--connect ml2 artdeco-button artdeco-button--2 artdeco-button--primary ember-view']")

            if len(button_connect) == 0:
                button_more = driver.find_elements_by_css_selector(
                    "[class='artdeco-dropdown artdeco-dropdown--placement-bottom artdeco-dropdown--justification-left ember-view']")
                button_more[0].click()
                sleep(sleep_time)

                button_connect = driver.find_elements_by_css_selector(
                    "[class='pv-s-profile-actions pv-s-profile-actions--connect pv-s-profile-actions__overflow-button full-width text-align-left artdeco-dropdown__item artdeco-dropdown__item--is-dropdown ember-view']")

            sleep(sleep_time)

            if len(button_connect) > 0:
                button_connect[0].click()

                sign_in_button = driver.find_elements_by_css_selector("[aria-label='Add a note']")
                sleep(sleep_time)
                sign_in_button[0].click()
                sleep(sleep_time)

                message = driver.find_element_by_id('custom-message')
                sleep(sleep_time)
                message.send_keys('Hi ' + first_name + invite_message)
                sleep(sleep_time)

                sign_in_button = driver.find_elements_by_css_selector("[aria-label='Send now']")
                sleep(sleep_time)
                sign_in_button[0].click()
                print('Invite sent...'+url_individual)
            else:
                print('Pending...')
            open('data/Anubis_urls_to_add/'+path+'/' + url_individual,
                 'a').close()
        else:
            print('Added...')
            open('data/Anubis_urls_to_add/'+path+'/' + url_individual,
                 'a').close()
    except:
        print('Error with url: ' + url_individual, ' ignore')



