# def install(package):
#     subprocess.check_call([sys.executable, "-m", "pip", "install", package])
# install('pymysql')
import importlib
import os

from time import sleep

import numpy
from selenium import webdriver
from bs4 import BeautifulSoup
from webdriver_manager.chrome import ChromeDriverManager
import pandas as pd

import linkedin_tools
importlib.reload(linkedin_tools)

user_name = 'jasonandriessen@outlook.com'
password = 'Jesus123'
invite_message = ' \n\nJason Andriessen from CoreData here - I publish research insights as frequently as I can about practice management, consumer behaviour and the future of financial planning - be great to connect with you. \n\nWith kind regards \nJason '
invite_search_key_words = 'Financial%20Adviser'
connection_list_csv='connections_to_add_FPA.csv'
path='connections_added_jason'

driver = linkedin_tools.login_to_linkedin(user_name,password)

connections_to_add = pd.read_csv("data/Anubis_urls_to_add/"+connection_list_csv, encoding='latin1')
url_finished = os.listdir('data/Anubis_urls_to_add/'+path+'/')
url_finished = [w.replace('.csv', '') for w in url_finished]

connections_to_add = connections_to_add[~ connections_to_add['linkedin_url'].isin(url_finished)]
connections_to_add = connections_to_add.reset_index()

sleep_time = 1

for p in range(0, 50):
    print('loop no:' + str(p))
    sleep(sleep_time)
    url_individual = connections_to_add.linkedin_url[p]
    linkedin_tools.add_connection_by_url(driver,url_individual,path,invite_message,sleep_time)
