# def install(package):
#     subprocess.check_call([sys.executable, "-m", "pip", "install", package])
# install('pymysql')
import importlib
import os
import requests
from parsel import Selector
from time import sleep
from selenium import webdriver
from bs4 import BeautifulSoup
from webdriver_manager.chrome import ChromeDriverManager
import pandas as pd
import numpy as np
import glob
import re
import linkedin_tools

importlib.reload(linkedin_tools)

user_name = 'andrew@coredata.com.au'
password = 'Kismet5898'



driver = linkedin_tools.login_to_linkedin(user_name, password)

# Populate URL ---------------------------

regex = re.compile('[^a-zA-Z]')

for i in range(0,200):
    connections = pd.read_csv("data/" + user_name + "/Connections.csv", encoding='utf-8')
    connections = connections.fillna('')

    url_finished = os.listdir("data/" + user_name + "/Connections_scraped/")
    url_finished = [w.replace('.csv', '') for w in url_finished]

    index = ((~ (connections['linkedin_url'].str.replace('/in/','').str.replace('/','')).isin(url_finished)) | (connections['linkedin_url'] == '' )) & (connections['linkedin_url'] != 'Error')
    if sum(index)>0:
        index = np.where(index)[0][0]

        first_name = connections['First Name'][index]
        last_name = connections['Last Name'][index]
        company = connections['Company'][index].split(' ')[0]
        last_name = regex.sub('', last_name.split(' ')[0])

        print(i)
        linkedin_url= connections['linkedin_url'][index]
        if linkedin_url == '':
            try:
                driver.get('https://www.linkedin.com/search/results/people/?facetNetwork=%5B%22F%22%5D&keywords='+first_name+
                           '%20'+last_name+'%20'+company+'&origin=FACETED_SEARCH')
                sleep(1)
                soup = BeautifulSoup(driver.page_source, 'html.parser')
                linkedin_url = soup.find_all('a', attrs={'class': 'search-result__result-link ember-view'})[0]['href']
            except:
                print('can not get by name company ')
        if linkedin_url == '':
            try:
                driver.get('https://www.linkedin.com/search/results/people/?facetNetwork=%5B%22F%22%5D&keywords=' +
                           first_name + '%20' + last_name + '%20&origin=FACETED_SEARCH')
                sleep(1)
                soup = BeautifulSoup(driver.page_source, 'html.parser')
                linkedin_url = soup.find_all('a', attrs={'class': 'search-result__result-link ember-view'})[0]['href']
            except:
                print('can not get by name ')
        if linkedin_url == '':
            try:
                driver.get('https://www.linkedin.com/search/results/people/?facetNetwork=%5B%22F%22%5D&keywords=' +
                           first_name + '%20' + company + '%20&origin=FACETED_SEARCH')
                sleep(1)
                soup = BeautifulSoup(driver.page_source, 'html.parser')
                linkedin_url = soup.find_all('a', attrs={'class': 'search-result__result-link ember-view'})[0]['href']
            except:
                print('can not get by name ')

        if linkedin_url == '':
            try:
                driver.get('https://www.linkedin.com/search/results/people/?facetNetwork=%5B%22F%22%5D&keywords=' +
                           first_name + '%20%20&origin=FACETED_SEARCH')
                sleep(1)
                soup = BeautifulSoup(driver.page_source, 'html.parser')
                linkedin_url = soup.find_all('a', attrs={'class': 'search-result__result-link ember-view'})[0]['href']
            except:
                print('can not get by name ')
        if linkedin_url !='':
            connections['linkedin_url'][index] = linkedin_url
        else:
            connections['linkedin_url'][index] = 'Error'


        if linkedin_url !='':
            # Scrape
            sleep_time=1
            try:
                linkedin_url_clean = linkedin_url.replace('/in/','').replace('/','')
                url_individual = 'https://www.linkedin.com/in/' + linkedin_url_clean
                driver.get(url_individual)

                sleep(1)
                driver.execute_script("window.scrollBy(0, 1000);")
                name = ''
                try:
                    soup = BeautifulSoup(driver.page_source, 'html.parser')
                    name = soup.find_all('li', attrs={'class': 'inline t-24 t-black t-normal break-words'})[0].text.strip()
                except:
                    print('no name ')
                connect = []
                try:
                    connect = driver.find_elements_by_css_selector("[aria-label='Connect with " + name + "']")
                except:
                    print('not new ')

                not_available = []
                try:
                    not_available = soup.find_all('div', attrs={'class': 'profile-unavailable'})[0].text.strip()
                except:
                    print('page available ')

                if len(connect) == 0 and len(not_available) == 0:
                    try:
                        sign_in_button = driver.find_elements_by_css_selector("[id='line-clamp-show-more-button']")
                        driver.execute_script("arguments[0].scrollIntoView();", sign_in_button[0])
                        driver.execute_script("window.scrollBy(0, -200);")
                        sleep(sleep_time)
                        sign_in_button[0].click()
                    except:
                        print("No about")
                    try:
                        sign_in_button = driver.find_elements_by_css_selector(
                            "[class='pv-profile-section__see-more-inline pv-profile-section__text-truncate-toggle link link-without-hover-state']")
                        driver.execute_script("arguments[0].scrollIntoView();", sign_in_button[0])
                        driver.execute_script("window.scrollBy(0, -200);")
                        sleep(sleep_time)
                        sign_in_button[0].click()
                    except:
                        print("no more experience")

                    try:
                        sign_in_button = driver.find_elements_by_css_selector(
                            "[class='pv-profile-section__see-more-inline pv-profile-section__text-truncate-toggle link link-without-hover-state']")
                        driver.execute_script("arguments[0].scrollIntoView();", sign_in_button[0])
                        driver.execute_script("window.scrollBy(0, -200);")
                        sleep(sleep_time)
                        sign_in_button[0].click()
                    except:
                        print("no more education")

                    try:
                        sign_in_button = driver.find_elements_by_css_selector("[class='inline-show-more-text__button link']")
                        for b in range(0, len(sign_in_button)):
                            driver.execute_script("arguments[0].scrollIntoView();", sign_in_button[b])
                            driver.execute_script("window.scrollBy(0, -200);")
                            sleep(sleep_time)
                            sign_in_button[b].click()
                    except:
                        print("no more show more details")

                    try:
                        sign_in_button = driver.find_elements_by_css_selector(
                            "[class='pv-profile-section__see-more-inline pv-profile-section__text-truncate-toggle link link-without-hover-state']")
                        for b in range(0, 1):
                            sleep(sleep_time)

                            driver.execute_script("arguments[0].scrollIntoView();", sign_in_button[b])

                            driver.execute_script("window.scrollBy(0, -200);")
                            sleep(sleep_time)
                            sign_in_button[b].click()
                    except:
                        print("no more  job details")

                    soup = BeautifulSoup(driver.page_source, 'html.parser')

                    name = soup.find_all('li', attrs={'class': 'inline t-24 t-black t-normal break-words'})[0].text.strip()
                    location_connection = \
                        soup.find_all('ul', attrs={'class': 'pv-top-card--list pv-top-card--list-bullet mt1'})[
                            0].text.strip()

                    about = soup.find_all('section', attrs={
                        'class': 'artdeco-container-card pv-profile-section pv-about-section artdeco-card ember-view'})[
                        0].text.strip()

                    try:
                        image_url = soup.find_all('img', attrs={'title': name})[0].attrs['src']
                        img_data = requests.get(image_url).content
                        with open('data/' + user_name + '/Connections_scraped_image/' + linkedin_url_clean + '.jpg', 'wb') as handler:
                            handler.write(img_data)
                    except:
                        print("no image")

                    experience_concat_all = ''
                    experience_section = soup.find_all('section', attrs={'id': 'experience-section'})
                    if len(experience_section) > 0:
                        experience_section_positions = experience_section[0].find_all('li', attrs={
                            'class': 'pv-entity__position-group-pager pv-profile-section__list-item ember-view'})
                        location = ''
                        for e in range(0, len(experience_section_positions)):
                            position_group = experience_section_positions[e].find_all('li', attrs={
                            'class': 'pv-entity__position-group-role-item'})
                            if len(position_group) != 0:
                                company = ''
                                company = experience_section_positions[e].find_all('h3', attrs={'class': 't-16 t-black t-bold'})[
                                    0].text.replace('Company Name', '').strip()
                                for g in range(0, len(position_group)):
                                    job_title = ''
                                    date_range = ''
                                    date_duration = ''
                                    location = ''
                                    extra_detail = ''

                                    job_title = position_group[g].find_all('h3', attrs={'class': 't-14 t-black t-bold'})[
                                        0].text.replace('Title', '').strip()
                                    date_range = position_group[g].find_all('h4', attrs={
                                        'class': 'pv-entity__date-range t-14 t-black--light t-normal'})[0].text.replace(
                                        'Dates Employed', '').strip()
                                    date_duration = \
                                        position_group[g].find_all('h4', attrs={'class': 't-14 t-black--light t-normal'})[
                                            0].text.replace('Employment Duration', '').strip()
                                    try:
                                        location = position_group[g].find_all('h4', attrs={
                                            'class': 'pv-entity__location t-14 t-black--light t-normal block'})[0].text.replace(
                                            'Location', '').strip()
                                    except:
                                        print("no location")
                                    try:
                                        extra_detail = position_group[g].find_all('div', attrs={
                                            'class': 'pv-entity__extra-details t-14 t-black--light ember-view'})[0].text.strip()
                                    except:
                                        print("no extra detail")
                                    experience_concat = 'Company: ' + company + " \nJob_title: " + job_title + " \nDate_range: " + date_range + " \nDate_duration: " + date_duration + " \nLocation: " + location + " \nDetail: " + extra_detail.strip()
                                    experience_concat_all = experience_concat_all + '\n|\n\n\n ' + experience_concat
                                    if e == 0 & g == 0:
                                        company_current = company
                                        job_title_current = job_title


                            else:
                                job_title = ''
                                date_range = ''
                                date_duration = ''
                                location = ''
                                extra_detail = ''
                                company = ''

                                company = experience_section_positions[e].find_all('p', attrs={
                                    'class': 'pv-entity__secondary-title t-14 t-black t-normal'})[0].text.strip()
                                job_title = experience_section_positions[e].find_all('h3', attrs={'class': 't-16 t-black t-bold'})[
                                    0].text.strip()
                                try:
                                    date_range = experience_section_positions[e].find_all('h4', attrs={
                                        'class': 'pv-entity__date-range t-14 t-black--light t-normal'})[0].text.replace(
                                        'Dates Employed', '').strip()
                                except:
                                    print("no date range")
                                try:
                                    date_duration = \
                                        experience_section_positions[e].find_all('span',
                                                                                 attrs={'class': 'pv-entity__bullet-item-v2'})[
                                            0].text.strip()
                                except:
                                    print("no  date_duration")
                                try:
                                    location = experience_section_positions[e].find_all('h4', attrs={
                                        'class': 'pv-entity__location t-14 t-black--light t-normal block'})[0].text.replace(
                                        'Location', '').strip()
                                except:
                                    print("no location")
                                try:
                                    extra_detail = experience_section_positions[e].find_all('p', attrs={
                                        'class': 'pv-entity__description t-14 t-black t-normal inline-show-more-text ember-view'})[
                                        0].text.strip()
                                except:
                                    try:
                                        extra_detail = experience_section_positions[e].find_all('p', attrs={
                                            'class': 'pv-entity__description t-14 t-black t-normal inline-show-more-text inline-show-more-text--is-collapsed ember-view'})[
                                            0].text.strip()
                                    except:
                                        print("no extra details")
                                experience_concat = 'Company: ' + company + " \nJob_title: " + job_title + " \nDate_range: " + date_range + " \nDate_duration: " + date_duration + " \nLocation: " + location + " \nDetail: " + extra_detail.strip()
                                experience_concat_all = experience_concat_all + '\n|\n\n\n ' + experience_concat
                                if e == 0:
                                    company_current = company
                                    job_title_current = job_title

                    education_section_concat = ''
                    education_section = soup.find_all('section', attrs={'id': 'education-section'})

                    if len(education_section) > 0:
                        education_section_school_list = education_section[0].find_all('h3', attrs={
                            'class': 'pv-entity__school-name t-16 t-black t-bold'})
                        education_section_degree_list = education_section[0].find_all('p', attrs={
                            'class': 'pv-entity__secondary-title pv-entity__degree-name t-14 t-black t-normal'})
                        education_section_field_of_study_list = education_section[0].find_all('p', attrs={
                            'class': 'pv-entity__secondary-title pv-entity__fos t-14 t-black t-normal'})
                        education_section_date_range = education_section[0].find_all('p', attrs={
                            'class': 'pv-entity__dates t-14 t-black--light t-normal'})
                        for e in range(0, len(education_section_school_list)):
                            fos = ''
                            school = education_section_school_list[e].text.strip()
                            degree = ''
                            fos = ''
                            date_range = ''
                            try:
                                degree = education_section_degree_list[e].text.replace('Degree Name', '').strip()
                            except:
                                print('No Degree')
                            try:
                                fos = education_section_field_of_study_list[e].text.replace('Field Of Study', '').strip()
                            except:
                                print("no fos")
                            try:
                                date_range = education_section_date_range[e].text.replace(
                                    'Dates attended or expected graduation',
                                    '').strip()
                            except:
                                print('No date range')
                            education_section_concat = education_section_concat + '\n|\n\n\n School: ' + school + '\nDegree: ' + degree + '\nField of Study: ' + fos + '\nDate_range: ' + date_range
                            if e == 0:
                                school_recent = school
                                degree_recent = degree
                                fos_recent = fos
                                date_range_recent = date_range
                    Licenses_certifications = ''
                    try:
                        Licenses_certifications = soup.find_all('section', attrs={'id': 'certifications-section'})[
                            0].text.strip()
                    except:
                        print('No certifications-section')

                    Skills_concat = ''
                    section_content = ''
                    section_content = soup.find_all('section', attrs={
                        'class': 'pv-profile-section pv-skill-categories-section artdeco-container-card artdeco-card first-degree ember-view'})
                    if len(section_content) > 0:
                        section_content_list = section_content[0].find_all('span', attrs={
                            'class': 'pv-skill-category-entity__name-text t-16 t-black t-bold'})
                        for s in range(0, len(section_content_list)):
                            content1 = section_content_list[s].text.strip()
                            Skills_concat = Skills_concat + '\n|\n\n\n Skill: ' + content1

                    Accomplishments_concat = ''
                    section_content = ''
                    section_content = soup.find_all('section', attrs={
                        'class': 'pv-profile-section pv-accomplishments-section artdeco-container-card artdeco-card ember-view'})
                    if len(section_content) > 0:
                        section_content_list = section_content[0].find_all('div', attrs={
                            'class': 'pv-accomplishments-block__content break-words'})
                        for s in range(0, len(section_content_list)):
                            content1 = section_content_list[s].text.strip()
                            Accomplishments_concat = Accomplishments_concat + '\n|\n\n\n Accomplishment: ' + content1

                    interests_concat = ''
                    section_content = ''
                    section_content = soup.find_all('section', attrs={
                        'class': 'pv-profile-section pv-interests-section artdeco-container-card artdeco-card ember-view'})
                    if len(section_content) > 0:
                        section_content_list = section_content[0].find_all('li', attrs={
                            'class': 'pv-interest-entity pv-profile-section__card-item ember-view'})
                        for s in range(0, len(section_content_list)):
                            content1 = section_content_list[s].text.strip()
                            interests_concat = interests_concat + '\n|\n\n\n Interest: ' + content1

                    # contact info
                    sign_in_button = driver.find_elements_by_css_selector(
                        "[data-control-name='contact_see_more']")
                    driver.execute_script("arguments[0].scrollIntoView();", sign_in_button[0])
                    driver.execute_script("window.scrollBy(0, -200);")
                    sleep(sleep_time)
                    sign_in_button[0].click()
                    sleep(sleep_time)

                    soup = BeautifulSoup(driver.page_source, 'html.parser')

                    address = ''
                    phone = ''
                    email = ''
                    website = ''
                    twitter = ''
                    try:
                        twitter = soup.find_all('section', attrs={'class': 'pv-contact-info__contact-type ci-twitter'})[
                            0].text.replace(
                            'Twitter', '').strip()
                    except:
                        print("no twitter")
                    try:
                        address = soup.find_all('section', attrs={'class': 'pv-contact-info__contact-type ci-address'})[
                            0].text.replace(
                            'Address', '').strip()
                    except:
                        print("no address")
                    try:
                        phone = soup.find_all('section', attrs={'class': 'pv-contact-info__contact-type ci-phone'})[
                            0].text.replace(
                            'Phone', '').strip()
                    except:
                        print("no phone")
                    try:
                        email = soup.find_all('section', attrs={'class': 'pv-contact-info__contact-type ci-email'})[
                            0].text.replace('Email', '').strip()
                    except:
                        print("no email")
                    try:
                        website = soup.find_all('section', attrs={'class': 'pv-contact-info__contact-type ci-websites'})[
                            0].text.replace('Website', '').strip()
                    except:
                        print("no website")

                    data = [[name, company_current,
                             job_title_current, email,
                             phone, address, website, twitter, location_connection,
                             about, experience_concat_all, education_section_concat,
                             Licenses_certifications,
                             Skills_concat,
                             Accomplishments_concat
                                , interests_concat
                             ]]

                    df = pd.DataFrame(data,
                                      columns=['name', 'company', 'job_title', 'email', 'phone', 'address', ' website',
                                               'twitter',
                                               'location_connection', 'about',
                                               'experience_section_company_list', 'education_section_concat',
                                               'Licenses_certifications',
                                               'skills_section',
                                               'accomplishments_section'
                                          , 'interests_section'
                                               ])
                    df.to_csv('data/' + user_name + '/Connections_scraped/' + linkedin_url_clean + '.csv')
                    sleep(1)
            except:
                print("Error with url : " + linkedin_url_clean)
                connections['linkedin_url'][index] = 'Error'
        connections.to_csv("data/" + user_name + "/Connections.csv", index=False, encoding='utf-8-sig')
    else:
        print('finished all ')

# merge all csvs ----
path = r'data/' + user_name + '/Connections_scraped' # use your path
all_files = glob.glob(path + "/*.csv")
df_merge = pd.DataFrame()

for filename in all_files:
    df = pd.read_csv(filename, index_col=None, header=0)
    df['linkedin_url'] = filename.replace('data/' + user_name + '/Connections_scraped\\','').replace('.csv','')
    df_merge = df_merge.append(df)
df_merge.to_csv('data/' + user_name + '/Connections_scraped_merged.csv',index=False)

